/**********
Copyright (c) 2018, Xilinx, Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software
without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********/
#include <vector>
#include <iostream>
#include "/opt/Xilinx/Vivado/2019.1/include/gmp.h"
#define AP_INT_MAX_W 4096
#define FIRST_NUM @FIRST_NUM@
#include <ap_int.h>

static const int DATA_SIZE = 4;

static const std::string error_message =
    "Error: Result mismatch:\n"
    "i = %d CPU result = %d Device result = %d\n";
extern "C" {
void  mul( ap_uint<512> *first_num, ap_uint<512> *second_num, ap_uint<512> *result);
}
void base_decomposition(ap_uint<512> *decomposition, ap_uint<2048> &n)
{
        for (int i=0; i< 2048/512; i++)
        {
                decomposition[i] = n.range(512-1+512*i,512*i);
        }
        //decomposition[AP_INT_MAX_W/512]=n.range(AP_INT_MAX_W-1,(AP_INT_MAX_W/512)*512);
}

void base_recomposition(ap_uint<4096> &acc,ap_uint<512> *decomposition)
{


                for (int i=0; i< 4096/512; i++)
                {
                        acc.range(512-1+512*i,512*i)= decomposition[i];
                }
                //acc.range(AP_INT_MAX_W-1,(AP_INT_MAX_W/512)*512) = decomposition[AP_INT_MAX_W/512];
}











int main(int argc, char* argv[])
{
    //Allocate Memory in Host Memory
    //size_t size_bytes = DATA_SIZE * sizeof(ap_uint<512>);
    ap_uint < 512 > source_a[DATA_SIZE];
    ap_uint < 512 > source_b[DATA_SIZE];
    ap_uint < 512 > source_results[2*DATA_SIZE];

    //init out result
    for (int i=0;i< 2*DATA_SIZE;i++)
        source_results[i]=0;


    gmp_randstate_t state;
    gmp_randinit_default(state);
    mpz_t rnd, a_gmp,b_gmp;
    mpz_init(a_gmp);
    mpz_init(b_gmp);
    char * num_str;
    ap_uint<2048> a,b;

    mpz_urandomb(a_gmp,state,2048);
    num_str = mpz_get_str(NULL,10,a_gmp);
    //a.fromString(num_str,strlen(num_str),10);
    a = ap_uint<2048>(num_str);
    base_decomposition(&(source_a[0]),a);

    mpz_urandomb(b_gmp,state,2048);
    num_str = mpz_get_str(NULL,10,b_gmp);
    //b.fromString(num_str,strlen(num_str),10);
    b = ap_uint<2048>(num_str);
    base_decomposition(&(source_b[0]),b);
    // Copy image host buffer



    mul( &(source_a[0]), &(source_b[0]), &(source_results[0]));
    //Launch the Kernel

for (int i=0; i<2*DATA_SIZE; i++)
	std::cout<<"after pos: res["<<i <<"] value: "<<source_results[i]<<std::endl;
    ap_uint <4096> res;
    base_recomposition(res,source_results);
    int match = 0;
#if FIRST_NUM == 2048
    ap_uint<2048> a_1 = a;
    ap_uint<2048> b_1 = b;
#elif FIRST_NUM == 1024
    ap_uint<1024> a_1=a.range(1023,0);
    ap_uint<1024> b_1=b.range(1023,0);
#elif FIRST_NUM == 512
    ap_uint<512> a_1=a.range(511,0);
    ap_uint<512> b_1=b.range(511,0);
#elif FIRST_NUM == 256
    ap_uint<256> a_1=a.range(255,0);
    ap_uint<256> b_1=b.range(255,0);
#elif FIRST_NUM == 128
    ap_uint<128> a_1=a.range(127,0);
    ap_uint<128> b_1=b.range(127,0);
#else
#error "no value for FIRST_NUM: dimension of number undefined"
#endif
    ap_uint<4096> result =a_1*b_1;
    if (result != res) {
    	    std::cout<<"result sw is: "<<result<<std::endl;
	    std::cout<<"result hw is: "<<res<<std::endl;

	    std::cout << "TEST FAILED" << std::endl;
	return EXIT_FAILURE;
        }

    std::cout<<"result sw is: "<<result<<std::endl;
    std::cout<<"result hw is: "<<res<<std::endl;

    std::cout << "TEST PASSED" << std::endl;
    return EXIT_SUCCESS;

}
