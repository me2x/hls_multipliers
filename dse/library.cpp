
#define AP_INT_MAX_W 4097

#define FIRST_NUM @FIRST_NUM@
#define SECOND_NUM @SECOND_NUM@
#define THIRD_NUM @THIRD_NUM@ 
#define KARAT_SIZE 64
#define NUM_COMBAS @NUM_COMBAS@
#define NUM_MULS @NUM_MULS@
#define LV0 @LV0@
#define LV1 @LV1@
#define LV2 @LV2@
#define LV3 @LV3@
#define LV4 @LV4@


#include "/opt/Xilinx/Vivado/2019.1/include/gmp.h"
#include "ap_int.h"


#if 0
//utility functions. needed to divide and rebuild 2048 bits number working with incoming/outgoing 512 bit_width axi bus.
/* __attribute__((always_inline)) ap_uint<2048> my_memcpy_2048 (ap_uint<512> * data)
{
        ap_uint<2048> result;
        result.range(511,0) = data[0];
        result.range(1023,512) = data[1];
        result.range(1535,1024) = data[2];
        result.range(2047,1536) = data[3];
        return result;
}*/
/* __attribute__((always_inline)) */void my_memcpy_2048 (ap_uint<512> * data, ap_uint<2048>& result)
{

    	result.range(511,0) = data[0];
    	result.range(1023,512) = data[1];
    	result.range(1535,1024) = data[2];
    	result.range(2047,1536) = data[3];
}

/*__attribute__((always_inline))*/ void my_return_cpy_2048 (ap_uint<512> * data, ap_uint<2048>& result)
{
        data[0]=result.range(511,0);
        data[1]=result.range(1023,512);
        data[2]=result.range(1535,1024);
        data[3]=result.range(2047,1536);
}

 __attribute__((always_inline)) void my_memcpy_1024 (ap_uint<512> * data, ap_uint<1024>& result)
{

    	result.range(511,0) = data[0];
    	result.range(1023,512) = data[1];
}

__attribute__((always_inline)) void my_return_cpy_1024 (ap_uint<512> * data, ap_uint<1024>& result)
{
        data[0]=result.range(511,0);
        data[1]=result.range(1023,512);
}
//templates of the library.
#endif
// template to divide the number in chunks. both class T and Y must be ap_uint, with different related sizes.
template<class T, class Y, int chunk_id, int chunk_size>
struct num_splitter{
	__attribute__((always_inline)) inline static void split(const T input_num, Y* out_vec)
	{
		//#pragma HLS inline
		out_vec[chunk_id]=input_num.range((chunk_id+1)*(chunk_size)-1,chunk_id*(chunk_size));
		num_splitter<T,Y,chunk_id-1,chunk_size>::split(input_num,out_vec);
	}
};


template<class T, class Y, int chunk_size>
struct num_splitter<T,Y,0,chunk_size>
{
	__attribute__((always_inline)) inline static void split(const T input_num, Y* out_vec)
	{
		//#pragma HLS inline
		out_vec[0]=input_num.range(chunk_size-1,0);
	}
};

// templates for the "unrolling" of the matrix. all the cells are instantiated.
template<class T, class Y, int num_muls ,int row_value, int col_value>
struct horizontal_unroll
{
	__attribute__((always_inline)) inline static void do_job(T partial_products[num_muls][num_muls], Y first_num[num_muls], Y second_num[num_muls])
	{
//#pragma HLS inline
		partial_products[row_value][col_value]=first_num[row_value]*second_num[col_value];
		horizontal_unroll<T,Y,num_muls,row_value,col_value-1>::do_job(partial_products,first_num,second_num);
	}
};

template<class T, class Y, int num_muls, int row_value>
struct horizontal_unroll<T,Y,num_muls, row_value,0>
{
	__attribute__((always_inline)) inline static void do_job(T partial_products[num_muls][num_muls], Y first_num[num_muls], Y second_num[num_muls])
	{
//#pragma HLS inline
		partial_products[row_value][0]=first_num[row_value]*second_num[0];
	}
};

template<class T, class Y, int num_muls, int row_value>
struct vertical_unroll
{
	__attribute__((always_inline)) inline static void do_job(T partial_products[num_muls][num_muls], Y first_num[num_muls], Y second_num[num_muls])
	{
//#pragma HLS inline
		//partial_products[row_value][col_value]=first_num[row_value]*second_num[col_value];
		vertical_unroll<T,Y,num_muls,row_value-1>::do_job(partial_products,first_num,second_num);
		horizontal_unroll<T,Y,num_muls,row_value,num_muls-1>::do_job(partial_products,first_num,second_num);
	}
};

template<class T, class Y, int num_muls>
struct vertical_unroll<T,Y,num_muls,0>
{
	__attribute__((always_inline)) inline static void do_job(T partial_products[num_muls][num_muls], Y first_num[num_muls], Y second_num[num_muls])
	{
//#pragma HLS inline
		horizontal_unroll<T,Y,num_muls,0,num_muls-1>::do_job(partial_products,first_num,second_num);
	}
};









//template called by the expanders to build the column sum in comba algorithm
template<class Y, class T, int size, int max_value, int min_value, int term_index > // the first term_index is equals to the number of terms to sum minus one
struct inner_unroll
{
	__attribute__((always_inline)) inline static Y compute(const T matrix[size][size])
	{

		return matrix[max_value - term_index][min_value + term_index] + inner_unroll<Y,T,size,max_value,min_value,term_index-1>::compute(matrix);
	}
};

template<class Y, class T, int size, int min_value, int max_value >
struct inner_unroll<Y,T,size,max_value,min_value,0>
{
	__attribute__((always_inline)) inline static Y compute(const T matrix[size][size])
	{

		return matrix[max_value][min_value];
	}
};

//templates used to produce the diagonals
template< class T, class Y, int size, int idx >
struct right_expander
{
	__attribute__((always_inline)) inline static void combine(const T input_matrix[size][size], Y output_vec[2*size -1])
	{
		//#pragma HLS inline
		right_expander<T,Y,size,idx-1>::combine(input_matrix,output_vec);
		output_vec[ idx ] = inner_unroll<Y,T,size,idx,0,idx>::compute(input_matrix);

	}
};

template< class T, class Y, int size >
struct right_expander<T,Y,size,0>
{
	__attribute__((always_inline)) inline static void combine(const T input_matrix[size][size], Y output_vec[2*size - 1])
	{
		//#pragma HLS inline
		output_vec[ 0 ] = inner_unroll<Y,T,size,0,0,0>::compute(input_matrix);
	}
};


template< class T, class Y, int size, int idx >
struct left_expander
{
	__attribute__((always_inline)) inline static void combine(const T input_matrix[size][size], Y output_vec[2*size -1])
	{
		//#pragma HLS inline
		output_vec[ 2*size - 2 - idx ] = inner_unroll<Y,T,size,size-1,size-idx-1,idx>::compute(input_matrix);
		left_expander<T,Y,size,idx-1>::combine(input_matrix,output_vec);
	}
};

template< class T, class Y, int size >
struct left_expander<T,Y,size,0>
{
	__attribute__((always_inline)) inline static void combine(const T input_matrix[size][size], Y output_vec[2*size - 1])
	{
		//#pragma HLS inline
		output_vec[ 2*size - 2 ] = inner_unroll<Y,T,size,size-1,size-1,0>::compute(input_matrix);
	}
};

//actually the only one called among those that build the diagonals. uses left/right expanders and the inner unroll
template< class T, class Y, int size >
__attribute__((always_inline)) inline void compute_diagonal_summ( const T input_matrix[size][size], Y output_vec[2*size - 1])
{
	//#pragma HLS inline
	right_expander<T,Y,size,size-2>::combine(input_matrix, output_vec);
	output_vec[size-1] = inner_unroll<Y,T,size,size-1,0,size-1>::compute(input_matrix);
	left_expander<T,Y,size,size-2>::combine(input_matrix, output_vec);
}




//templates to produce the outgoing numbers. uses the diagonal sums produced to create "word_sized" temporary variables containing the exacts bits
template <class T, class Y, int word_size, int safety_bits, int chunk_id>
struct final_adders
{
	__attribute__((always_inline)) inline static void create_num(const T* pp, Y* tmp)
	{
//#pragma HLS inline
		final_adders<T,Y,word_size,safety_bits,chunk_id-1>::create_num(pp,tmp);
		tmp[chunk_id] = tmp[chunk_id-1].range(word_size+1,word_size)+pp[chunk_id].range(word_size-1,0)+pp[chunk_id-1].range(2*word_size-1,word_size)+pp[chunk_id-2].range(2*word_size+safety_bits-1,2*word_size);
	}
};
template <class T, class Y,  int word_size, int safety_bits>
struct final_adders<T, Y, word_size, safety_bits, 1>
{
	//last instantiation (first 2 "words") first word doesnt have adds, so just take lowest bits, second one doesn't have carry, just sum the bits.
	//note that the second word doesn't have to manage safety bits, those are added to avoid the loss of carries in the previous operation (diagonal sums)
	__attribute__((always_inline)) inline static void create_num(const T* pp, Y* tmp)
	{
//#pragma HLS inline
	//	tmp[0] = pp[0].range(word_size-1,0);
	//	tmp[1] = pp[1].range(word_size-1,0)+pp[0].range(2*word_size-1,word_size);
	}
};
template <class T, class Y,  int word_size,int safety_bits, int chunk_id>
__attribute__((always_inline)) inline void start_final_adders(const T* pp, Y* tmp)
{
//#pragma HLS inline
	//first instantiation (last partial product): only requires bits carry from id-2 and most significant bits from id-1. doesnt generate a carry.
		//final_adders<T,Y,word_size,safety_bits,chunk_id-1>::create_num(pp,tmp );
		tmp[chunk_id] = tmp[chunk_id-1].range(word_size+1,word_size)+(pp[chunk_id-1].range(2*word_size-1,word_size))+(pp[chunk_id-2].range(2*word_size+safety_bits-1,2*word_size));
};




//takes the "word_sized" variables and rebuild the big integer
template <class T,class Y, int word_size, int chunk_id>
struct rebuilder
{
	__attribute__((always_inline)) inline static void rebuild_num(const T* tmp, const Y* final_number)
		{

				rebuilder<T,Y,word_size,chunk_id-1>::rebuild_num(tmp,final_number);
				final_number->range(((chunk_id+1)*word_size)-1,(chunk_id)*word_size)=tmp[chunk_id].range(word_size-1,0);
		}
};
template <class T,class Y, int word_size>
struct rebuilder<T,Y,word_size,0>
{
	__attribute__((always_inline)) inline static void rebuild_num(const T* tmp, const Y* final_number)
		{

			final_number->range(word_size-1,0)=tmp[0].range(word_size-1,0);
		}
};


template <unsigned int x>
struct SignificantBits {
    static const unsigned int n = SignificantBits<x/2>::n + 1;
};

template <>
struct SignificantBits<0> {
    static const unsigned int n = 0;
};


//templated comba multiplication.
template <int num_size, int word_size,int num_muls, int dword = 2*word_size, int out_size =2*num_size, int num_words = num_size/word_size>
struct comba_mul
{
	static void do_job (ap_uint<num_size> A, ap_uint<num_size> B, ap_uint<out_size>* out ) //inlining this creates area problems for sure, not sure about forcing
	{

	#pragma HLS allocation instances=mul limit=num_muls operation
//	#ifdef PIPELINED
	#pragma HLS pipeline
//	#endif
		ap_uint<dword> partial_products[num_words][num_words];
		ap_uint<word_size> first_num[num_words],second_num[num_words];
	//	std::cout<<std::endl<<std::endl<<"comba sizes: numsize"<< num_size<<" word_size: "<<word_size<<" dword"<<dword <<" out_size"<<out_size<<" num_words"<<num_words<<std::endl;
	//	std::cout<<"values are: A: "<<A<<" and B: "<<B<<std::endl;
		#pragma HLS ARRAY_PARTITION variable=partial_products complete//block factor=div_result// dim=0
		#pragma HLS ARRAY_PARTITION variable=first_num complete
		#pragma HLS ARRAY_PARTITION variable=second_num complete

//		for (int i = 0; i <num_words;i++)
//			std::cout <<first_num[i] <<std::endl;
//		for (int i = 0; i <num_words;i++)
//			std::cout <<second_num[i] <<std::endl;
//		std::cout<<"init"<<std::endl;
		num_splitter<ap_uint<num_size>,ap_uint<word_size>,num_words-1,word_size>::split(A,first_num); //works with array index, need to start from chunk_id num_words
		num_splitter<ap_uint<num_size>,ap_uint<word_size>,num_words-1,word_size>::split(B,second_num);
//		for (int i = 0; i <num_words;i++)
//			std::cout<<"a["<<i<<"]: "<<first_num[i] <<std::endl;
//		for (int i = 0; i <num_words;i++)
//			std::cout <<"b["<<i<<"]: "<<second_num[i] <<std::endl;
		vertical_unroll<ap_uint<dword>,ap_uint<word_size>,num_words,num_words-1>::do_job(partial_products,first_num,second_num);

//		for (int i = 0; i <num_words;i++)
//			for (int j = 0; j <num_words;j++)
//				std::cout<<"partial product["<<i<<"]["<<j<<"] is: "<<partial_products[i][j]<<std::endl;
		ap_uint<out_size> final_number=0;
//		std::cout<<"Significant Bits: "<<SignificantBits<num_words>::n<<std::endl;
//		std::cout<<"pp size is: "<<dword+SignificantBits<num_words>::n<<std::endl;
		ap_uint<dword+SignificantBits<num_words>::n> pp [2*num_words-1]; //extra bits: log 2 of max amount of sums to be computed, which is the lenght of a diagonal of the matrix.
		ap_uint<word_size+2> tmp [2*num_words]; //2 extra bit for carry. max sum is 2*wordsize numbers and other 2 carry from smaller numbers, so i just need 1 extra bit for both of them.
		#pragma HLS ARRAY_PARTITION variable=pp complete//cyclic factor=8
		#pragma HLS ARRAY_PARTITION variable=tmp complete//cyclic factor=8

		#pragma HLS resource variable=partial_products core=MulnS
//		#pragma HLS resource variable=pp core=AddSubnS latency=8
//		#pragma HLS resource variable=tmp core=AddSubnS latency=8


		compute_diagonal_summ<ap_uint<dword>,ap_uint<dword+SignificantBits<num_words>::n>,num_words>(partial_products,pp);

//		for (int i = 0; i <2*num_words-1;i++)
//			std::cout<<"pp["<<i<<"] is: " <<pp[i] <<std::endl;

		tmp[0] = pp[0].range(word_size-1,0);
		tmp[1] = pp[1].range(word_size-1,0)+pp[0].range(dword-1,word_size);
		final_adders<ap_uint<dword+SignificantBits<num_words>::n>,ap_uint<word_size+2>, word_size,SignificantBits<num_words>::n,2*num_words-2>::create_num(pp,tmp );
		start_final_adders<ap_uint<dword+SignificantBits<num_words>::n>,ap_uint<word_size+2>, word_size,SignificantBits<num_words>::n,2*num_words-1>(pp, tmp);

//		for (int i = 0; i <2*num_words;i++)
//			std::cout<<"tmp["<<i<<"] is: " <<tmp[i] <<std::endl;


		rebuilder< ap_uint<word_size+2>, ap_uint<out_size>, word_size, 2*num_words-1>::rebuild_num (tmp, &final_number);
//		std::cout<<"final number is: "<<final_number<<std::endl;
		*out = final_number;
	}
};

template <int num_size, int word_size, int num_muls, int dsize, int out_size>
struct comba_mul <num_size, word_size, num_muls, dsize, out_size,1>
	{
	 static void do_job (ap_uint<num_size> A, ap_uint<num_size> B,ap_uint<out_size>* out) //special case where num_size=word_size
	{
	#pragma HLS INLINE OFF
		ap_uint<out_size> final_number=0;
		final_number = A*B;
		*out = final_number;
	}

	};

#if 0
template<int bw, int word_size,int iternum=bw/(word_size*2)>
struct adderb_halfbit
{
	 static inline void do_job (ap_uint<final_bw> z0, ap_uint<final_bw> z1,ap_uint<final_bw> z2,ap_uint<2*final_bw>* out, ap_uint<2> * carrys)
	{
		//recursion
		adderb_halfbit<bw,word_size,itrenum-1>::do_job(z0,z1,z2,out,carrys);
		//do stuff
	}	 
};


template<int final_bw, int word_size>
struct adderb_halfbit<final_bw,word_size,0>
{
	 static inline void do_job (ap_uint<final_bw> z0, ap_uint<final_bw> z1,ap_uint<final_bw> z2,ap_uint<2*final_bw>* out, ap_uint<2> * carrys) 
};
#endif

//templated karatsuba-comba, first template argument is max dimension of numbers to multiply, second is the dimension that is used to switch from karatsuba to combat, third is the comba word size.
template <int level, int name, int bit_width, int min_bit_width, int word_size,int comba_word_size, int num_combas, int num_muls, int actual_lvl, int... Ts>
struct karacomba
{
	static constexpr int num_words = bit_width/(word_size*2);
/*	__attribute__((always_inline)) static inline*/ static void do_job (ap_uint<bit_width> A, ap_uint<bit_width> B, ap_uint<2*bit_width>* out)
	{
	#pragma HLS INLINE OFF
#pragma HLS pipeline
        ap_uint<bit_width/2> ah, bh, al, bl, s1, s2,sub1,sub2,subl1,subl2;
        ah = A.range(bit_width-1,bit_width/2);
        bh = B.range(bit_width-1,bit_width/2);
        al = A.range((bit_width/2)-1,0);
        bl = B.range((bit_width/2)-1,0);
        bool value = (ah>al)xor(bh>bl); //optimization for having the sum not overflowing.
	if (ah > al)
	{
		//s1 = ah - al;
		sub1 = ah;
		subl1 = al;
	}
	else
	{
		//s1 = al - ah;
		sub1 = al;
		subl1 = ah;
	}
	if (bh > bl)
	{
		//s2 = bh - bl;
		sub2 = bh;
		subl2 = bl;
	}
	else
	{
		//s2 = bl - bh;
		sub2 = bl;
		subl2 = bh;
	}
		#pragma HLS resource variable=s1  core=AddSubnS latency=@stages@
	s1 = sub1 - subl1;
		#pragma HLS resource variable=s2  core=AddSubnS latency=@stages@
	s2 = sub2 - subl2;

        ap_uint<bit_width> z0,z1,z2,tmp;
	{
        #pragma HLS allocation instances=dojob limit=actual_lvl function
	#pragma HLS allocation instances=do_job.1 limit=actual_lvl function
	#pragma HLS allocation instances=do_job.2 limit=actual_lvl function
	#pragma HLS allocation instances=do_job.3 limit=actual_lvl function
	#pragma HLS allocation instances=do_job.4 limit=actual_lvl function
	#pragma HLS allocation instances=do_job.5 limit=actual_lvl function
	karacomba<level-1, name+1, bit_width/2,min_bit_width,word_size,comba_word_size,num_combas,num_muls,Ts...>::do_job(al, bl, &z0);
        karacomba<level-1, name+1, bit_width/2,min_bit_width,word_size,comba_word_size,num_combas,num_muls,Ts...>::do_job(ah, bh, &z2);
        karacomba<level-1, name+1, bit_width/2,min_bit_width,word_size,comba_word_size,num_combas,num_muls,Ts...>::do_job(s1, s2, &z1);
}
        ap_uint<bit_width*2> result;
		result.range(bit_width/2-1,0) = z0.range(bit_width/2-1,0);

		ap_uint<bit_width+2> middle_carry_init = static_cast<ap_uint<bit_width+2> >(z2) + z0;
		#pragma HLS resource variable=middle_carry_init  core=AddSubnS latency=@stages@
		ap_uint<bit_width+2> middle_carry_two = middle_carry_init + (z0>>bit_width/2);

		#pragma HLS resource variable=middle_carry_two  core=AddSubnS latency=@stages@
		ap_uint<bit_width+2> middle_carry_t = middle_carry_two + z1;
		#pragma HLS resource variable=middle_carry_t  core=AddSubnS latency=@stages@
		ap_uint<bit_width+2> middle_carry_f = middle_carry_two - z1;
		#pragma HLS resource variable=middle_carry_f  core=AddSubnS latency=@stages@
		ap_uint<bit_width+2> middle_carry = value ? middle_carry_t : middle_carry_f;
		//bits 2047-1024 (aka bit_width-1, bit_width/2)
		
		//attempt to decompose the sums into smaller components to pipeline them. it was suboptimal, but could be investigated more
/*		ap_uint<2> carry =0;
		for (int i=0; i<num_words;i++)
		{
			#pragma HLS PIPELINE
			ap_uint<word_size+2> tmp1 = z0.range(word_size*(i+1)-1,word_size*i) +z2.range(word_size*(i+1)-1,word_size*i) ;
			ap_uint<word_size+2> tmp2;
			if (value)
				tmp2 = tmp1 +z1.range(word_size*(i+1)-1,word_size*i);
			else
				tmp2 = tmp1 -z1.range(word_size*(i+1)-1,word_size*i);
			ap_uint<word_size+2> tmp3 = tmp2 + z0.range(bit_width/2-1+word_size*(i+1),bit_width/2+word_size*(i))+carry;
			result.range(bit_width/2-1+word_size*(i+1),bit_width/2+word_size*(i))= tmp3.range(word_size-1,0);
			carry= tmp3.range(word_size+1,word_size);
		}	

		//bits 3071 -2048 (aka bit_width+bit_width/2-1, bit_width)
		for (int i=0; i<num_words;i++)
		{
			#pragma HLS PIPELINE
			ap_uint<word_size+2> tmp1 = z0.range(bit_width/2+word_size*(i+1)-1,bit_width/2+word_size*i) +z2.range(bit_width/2+word_size*(i+1)-1,bit_width/2+word_size*i) ;
			ap_uint<word_size+2> tmp2;
			if (value)
				tmp2 = tmp1 +z1.range(bit_width/2+word_size*(i+1)-1,bit_width/2+word_size*i);
			else
				tmp2 = tmp1 -z1.range(bit_width/2+word_size*(i+1)-1,bit_width/2+word_size*i);
			ap_uint<word_size+2> tmp3 = tmp2 + z2.range(word_size*(i+1)-1,word_size*(i))+carry;
			result.range(bit_width+word_size*(i+1)-1,bit_width+word_size*(i))= tmp3.range(word_size-1,0);
			carry= tmp3.range(word_size+1,word_size);
		}

		//bits 4095-3072 (aka 2*bitwidth-1, 3/2*bitwidth)
		for (int i=0; i<num_words;i++)
		{
			#pragma HLS PIPELINE
			ap_uint<word_size+2> tmp = z2.range(bit_width/2+word_size*(i+1)-1,bit_width/2+word_size*(i))+carry;
			result.range(bit_width+bit_width/2+word_size*(i+1)-1,bit_width+bit_width/2+word_size*(i))= tmp.range(word_size-1,0);
			carry= tmp.range(word_size+1,word_size);
		}
*/
//		#pragma HLS resource variable=middle_carry  core=AddSubnS latency=@stages@
		result.range(bit_width-1,bit_width/2) = middle_carry.range(bit_width/2-1,0);
		#pragma HLS resource variable=tmp  core=AddSubnS latency=@stages@
        	tmp = z2+(middle_carry>>(bit_width/2));
	        result.range(2*bit_width-1,bit_width) = tmp;
//		result.range(2*bit_width-1,bit_width) = z2+(middle_carry>>(bit_width/2));

        *out = result;
	}
};



template <int level,int name, int bit_width, int word_size,int comba_word_size,int num_combas, int num_muls, int actual_lvl, int... Ts>
struct karacomba<level, name, bit_width,bit_width, word_size,comba_word_size,num_combas, num_muls,actual_lvl,Ts...>
{
	static constexpr int num_words = bit_width/(word_size*2);
/*	__attribute__((always_inline)) static inline*/ static void do_job(ap_uint<bit_width> A, ap_uint<bit_width> B, ap_uint<2*bit_width>* out)
	{
	#pragma HLS INLINE OFF
	#pragma HLS pipeline
		ap_uint<bit_width/2> ah, bh, al, bl, s1, s2, sub1,sub2,subl1,subl2;
		ah = A.range(bit_width-1,bit_width/2);
		bh = B.range(bit_width-1,bit_width/2);
		al = A.range((bit_width/2)-1,0);
		bl = B.range((bit_width/2)-1,0);
		bool value = (ah>al)xor(bh>bl);

		if (ah > al)
		{
			//s1 = ah - al;
			sub1 = ah;
			subl1 = al;
		}
		else
		{
			//s1 = al - ah;
			sub1 = al;
			subl1 = ah;
		}
		if (bh > bl)
		{
			//s2 = bh - bl;
			sub2 = bh;
			subl2 = bl;
		}
		else
		{
			//s2 = bl - bh;
			sub2 = bl;
			subl2 = bh;
		}
		#pragma HLS resource variable=s1   core=AddSubnS latency=@stages@
		s1 = sub1 - subl1;
		#pragma HLS resource variable=s2   core=AddSubnS latency=@stages@
		s2 = sub2 - subl2;
		ap_uint<bit_width> z0,z1,z2,tmp;

//TODO test this, not sure is correct
		#pragma HLS allocation instances=do_job.1 limit=num_combas function
		#pragma HLS allocation instances=do_job.2 limit=num_combas function
		#pragma HLS allocation instances=do_job.3 limit=num_combas function
		#pragma HLS allocation instances=do_job.4 limit=num_combas function
		#pragma HLS allocation instances=do_job.5 limit=num_combas function
		comba_mul<bit_width/2,comba_word_size,num_muls>::do_job(al, bl, &z0);
		comba_mul<bit_width/2,comba_word_size,num_muls>::do_job(ah, bh, &z2);
		comba_mul<bit_width/2,comba_word_size,num_muls>::do_job(s1, s2, &z1);

		ap_uint<bit_width*2> result;
		result.range(bit_width/2-1,0) = z0.range(bit_width/2-1,0);
		
		ap_uint<bit_width+2> middle_carry_t = static_cast<ap_uint<bit_width+2> >(z1 + z2 + z0) +(z0>>bit_width/2);
		#pragma HLS resource variable=middle_carry_t  core=AddSubnS latency=@stages@
		ap_uint<bit_width+2> middle_carry_f = static_cast<ap_uint<bit_width+2> >((z2+ z0) - z1)+(z0>>bit_width/2);
		#pragma HLS resource variable=middle_carry_f  core=AddSubnS latency=@stages@
		ap_uint<bit_width+2> middle_carry = value ? middle_carry_t : middle_carry_f;
		//bits 2047-1024 (aka bit_width-1, bit_width/2)
		//
		//attempt to decompose the sums into smaller components to pipeline them. it was suboptimal, but could be investigated more
/*		ap_uint<2> carry =0;
		for (int i=0; i<num_words;i++)
		{
		#pragma HLS unroll
			ap_uint<word_size+2> tmp1 = z0.range(word_size*(i+1)-1,word_size*i) +z2.range(word_size*(i+1)-1,word_size*i) ;
			ap_uint<word_size+2> tmp2;
			if (value)
				tmp2 = tmp1 +z1.range(word_size*(i+1)-1,word_size*i);
			else
				tmp2 = tmp1 -z1.range(word_size*(i+1)-1,word_size*i);
			ap_uint<word_size+2> tmp3 = tmp2 + z0.range(bit_width/2-1+word_size*(i+1),bit_width/2+word_size*(i))+carry;
			result.range((bit_width/2)+(word_size*(i+1))-1,(bit_width/2)+(word_size*i))= tmp3.range(word_size-1,0);
			carry= tmp3.range(word_size+1,word_size);
		}	

		//bits 3071 -2048 (aka bit_width+bit_width/2-1, bit_width)
		for (int i=0; i<num_words;i++)
		{
		#pragma HLS unroll
			ap_uint<word_size+2> tmp1 = z0.range(bit_width/2+word_size*(i+1)-1,bit_width/2+word_size*i) +z2.range(bit_width/2+word_size*(i+1)-1,bit_width/2+word_size*i) ;
			ap_uint<word_size+2> tmp2;
			if (value)
				tmp2 = tmp1 +z1.range(bit_width/2+word_size*(i+1)-1,bit_width/2+word_size*i);
			else
				tmp2 = tmp1 -z1.range(bit_width/2+word_size*(i+1)-1,bit_width/2+word_size*i);
			ap_uint<word_size+2> tmp3 = tmp2 + z2.range(word_size*(i+1)-1,word_size*(i))+carry;
			result.range(bit_width+word_size*(i+1)-1,bit_width+word_size*(i))= tmp3.range(word_size-1,0);
			carry= tmp3.range(word_size+1,word_size);
		}

		//bits 4095-3072 (aka 2*bitwidth-1, 3/2*bitwidth)
		for (int i=0; i<num_words;i++)
		{
		#pragma HLS unroll
			ap_uint<word_size+2> tmp = z2.range(bit_width/2+word_size*(i+1)-1,bit_width/2+word_size*(i))+carry;
			result.range(bit_width+bit_width/2+word_size*(i+1)-1,bit_width+bit_width/2+word_size*(i))= tmp.range(word_size-1,0);
			carry= tmp.range(word_size+1,word_size);
		}
*/
		result.range(bit_width-1,bit_width/2) = middle_carry.range(bit_width/2-1,0);
		#pragma HLS resource variable=tmp  core=AddSubnS latency=@stages@
        	tmp = z2+(middle_carry>>(bit_width/2));
	        result.range(2*bit_width-1,bit_width) = tmp;

		*out = result;

	}
};




/////// THIS IS THE LIBRARY INTERFACE /////////////
//the first two parameters are actually hacks that are needed to tell to vivado the order of the functions, since it uses alphabetical order.
//they have to be leaved fixed as in the example provided below
//the first one, level,  must be SignificantBits<FIRST_NUM/SECOND_NUM>::n
//the second one, name, must be 0 in the initialization
//the other parameters work as described in the paper
template <int level, int name, int bit_width, int min_bit_width, int word_size,int comba_word_size, int num_combas, int num_muls, int... Ts>
void multiplier(ap_uint<bit_width> A, ap_uint<bit_width> B, ap_uint<2*bit_width>* out)
{
        karacomba<level, name, bit_width, min_bit_width, word_size, comba_word_size, num_combas, num_muls, Ts...>::do_job(A,B,out);
}


//note that first param is bitwidht of input numbers.
//second is level of switch. numbers of karatsuba recursion is bitwidth/level of switch
//third is bitwidth of numbers in comba multipliers. cannot be equal or greater than level of switch.
//IF third = second/2, we have a pure karatsuba.
//if third = second/2^n con n>1 its a combined karatsuba-comba multiplication.

extern "C" {
void  mul( ap_uint<512>* first_num, ap_uint<512> *second_num, ap_uint<512>* result){
// Using Separate interface bundle gmem0 and gmem1 for both argument
// input and output. It will allow user to create two separate interfaces
// and as a result allow kernel to access both interfaces simultaneous.
#pragma HLS INTERFACE m_axi port=first_num  offset=slave bundle=gmem0 depth=4
#pragma HLS INTERFACE m_axi port=second_num offset=slave bundle=gmem1 depth=4
#pragma HLS INTERFACE m_axi port=result offset=slave bundle=gmem0 depth=8
#pragma HLS INTERFACE s_axilite port=first_num  bundle=control
#pragma HLS INTERFACE s_axilite port=second_num bundle=control
#pragma HLS INTERFACE s_axilite port=result bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control

//#pragma HLS dataflow
ap_uint<FIRST_NUM> test;// = A[0];
//test = first_num[0].range(63,0);
#if FIRST_NUM == 2048
//my_memcpy_2048(&first_num[0], test);
 test.range(511,0)     = first_num[0];
 test.range(1023,512)  = first_num[1];
 test.range(1535,1024) = first_num[2];
 test.range(2047,1536) = first_num[3];
#elif FIRST_NUM == 1024
 test.range(511,0)     = first_num[0];
 test.range(1023,512)  = first_num[1];
//my_memcpy_1024(&first_num[0], test);
#elif FIRST_NUM == 512
test = first_num[0];
#elif FIRST_NUM == 256
test = first_num[0].range(255,0);
#elif FIRST_NUM == 128
test = first_num[0].range(127,0);
#else
#error "no value for FIRST_NUM: dimension of number undefined"
#endif
//std::cout<<"a is: "<<test<<std::endl;
ap_uint<FIRST_NUM> test2;// = N[0];
#if FIRST_NUM == 2048
 test2.range(511,0)     = second_num[0];
 test2.range(1023,512)  = second_num[1];
 test2.range(1535,1024) = second_num[2];
 test2.range(2047,1536) = second_num[3];
//my_memcpy_2048(&second_num[0], test2);
#elif FIRST_NUM == 1024
//my_memcpy_1024(&second_num[0], test2);
 test2.range(511,0)     = second_num[0];
 test2.range(1023,512)  = second_num[1];
#elif FIRST_NUM == 512
 test2 = second_num[0];
#elif FIRST_NUM == 256
 test2 = second_num[0].range(255,0);
#elif FIRST_NUM == 128
 test2 = second_num[0].range(127,0);
#else
#error "no value for FIRST_NUM: dimension of number undefined"
#endif
//std::cout<<"b is: "<<test2<<std::endl;
ap_uint<FIRST_NUM*2> result_uint;
//std::cout<<"a is: "<<test<<std::endl<<"b is: "<<test2<<std::endl;

/////// THIS IS THE LIBRARY INTERFACE /////////////
//the first two parameters are actually hacks that are needed to tell to vivado the order of the functions, since it uses alphabetical order.
//they have to be leaved fixed as in the example provided below
//the first one, level,  must be SignificantBits<FIRST_NUM/SECOND_NUM>::n
//the second one, name, must be 0 in the initialization
//the other parameters work as described in the paper
multiplier<SignificantBits<FIRST_NUM/SECOND_NUM>::n,0,FIRST_NUM,SECOND_NUM,KARAT_SIZE,THIRD_NUM,NUM_COMBAS,NUM_MULS,LV0,LV1,LV2,LV3,LV4>(test,test2, &result_uint);

//ap_uint<FIRST_NUM> return_value = result_uint.range(FIRST_NUM-1,0);
//my_return_cpy (&out[4], test2);
#if FIRST_NUM == 2048
//std::cout<<"result_uint is: "<<return_value<<std::endl;
//my_return_cpy_2048(&result[0], return_value);
 result[0]=result_uint.range(511,0);
 result[1]=result_uint.range(1023,512);
 result[2]=result_uint.range(1535,1024);
 result[3]=result_uint.range(2047,1536);
 result[4]=result_uint.range(2559,2048);
 result[5]=result_uint.range(3071,2560);
 result[6]=result_uint.range(3583,3072);
 result[7]=result_uint.range(4095,3584);
//
#elif FIRST_NUM == 1024
//my_return_cpy_1024(&result[0], return_value);
 result[0]=result_uint.range(511,0);
 result[1]=result_uint.range(1023,512);
 result[2]=result_uint.range(1535,1024);
 result[3]=result_uint.range(2047,1536);
#elif FIRST_NUM == 512
 result[0]=result_uint.range(511,0);
 result[1]=result_uint.range(1023,512);
#elif FIRST_NUM == 256
 result[0]=result_uint;

#elif FIRST_NUM == 128
 result[0].range(255,0)=result_uint;
#else
#error "no value for FIRST_NUM: dimension of number undefined"
#endif


return;
}
}
